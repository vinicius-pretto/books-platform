import React from "react";
import { storiesOf } from "@storybook/react";
import Header from "../src/components/Header";

const stories = storiesOf("Header", module);

stories.add("Default", () => <Header />);
