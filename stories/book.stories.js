import React from "react";
import { storiesOf } from "@storybook/react";
import Book from "../src/components/Books/Book";

const stories = storiesOf("Book", module);

stories.add("Default", () => (
  <Book
    name="Eloquent Javascript"
    pictureUrl="http://localhost:6006/img/eloquent-javascript.svg"
  />
));
