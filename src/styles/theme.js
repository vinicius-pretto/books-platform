const theme = {
  primary: "#152939",
  link: "#7091E4",
  text: "#484848",
  gray: '#bfbaba',
  error: "#D21B1B"
};

export default theme;
