import React from 'react';
import styled from 'styled-components';

const ErrorText = styled.p`
  font-size: 13px;
  color: ${props => props.theme.error};
  margin-top: 5px;
`
ErrorText.displayName = 'ErrorText';

const ErrorMessage = ({ children }) => {
  return (
    <ErrorText>{children}</ErrorText>
  )
}

export default ErrorMessage;
