import React from "react";
import styled from "styled-components";

import Label from '../Label';
import ErrorMessage from '../ErrorMessage';
import FormGroup from '../FormGroup';

const StyledInput = styled.input`
  padding: 10px;
  border: 1px solid;
  border-radius: 4px;
  border-color: ${props => props.hasError ? props.theme.error : '#bfbaba'};
  outline-color: ${props => props.theme.primary};
`;
StyledInput.displayName = "StyledInput";

const Input = (props) => {
  const { label, id, type, name, value, placeholder, error, touched, onChange, onBlur } = props;
  const hasError = error && touched;

  return (
    <FormGroup>
      <Label htmlFor={id} hasError={hasError}>{label}</Label>
      <StyledInput
        id={id}
        type={type}
        name={name}
        placeholder={placeholder}
        onChange={onChange}
        onBlur={onBlur}
        value={value}
        hasError={hasError}
      />
      {hasError && <ErrorMessage>{error}</ErrorMessage>}
    </FormGroup>
  );
};

export default Input;
