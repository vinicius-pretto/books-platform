import React from "react";

import Input from "../Input";
import TextArea from "../TextArea";

import {
  FormContainer,
  Form,
  UploadContainer,
  UploadButton,
  ButtonContainer,
  SubmitButton,
  UploadedImage
} from "./style";

const BooksForm = props => {
  const {
    onChangeFile,
    uploadedFile,
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit
  } = props;

  return (
    <FormContainer onSubmit={handleSubmit}>
      <UploadContainer>
        {uploadedFile.src && (
          <UploadedImage src={uploadedFile.src} alt={uploadedFile.name} />
        )}
        <UploadButton
          id="cover"
          type="file"
          accept="image/png, image/jpeg, image/jpg, image/svg+xml"
          multiple={false}
          onChange={onChangeFile}
        />
      </UploadContainer>

      <Form>
        <Input
          id="name"
          type="text"
          name="name"
          label="Nome do livro"
          placeholder="Nome do livro"
          value={values.name}
          error={errors.name}
          touched={touched.name}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        <Input
          id="author"
          type="text"
          name="author"
          label="Autor"
          placeholder="Autor"
          value={values.author}
          error={errors.author}
          touched={touched.author}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        <Input
          id="language"
          type="text"
          name="language"
          label="Idioma"
          placeholder="Idioma"
          value={values.language}
          error={errors.language}
          touched={touched.language}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        <Input
          id="publisher"
          type="text"
          name="publisher"
          label="Editora"
          placeholder="Editora"
          value={values.publisher}
          error={errors.publisher}
          touched={touched.publisher}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        <Input
          id="year"
          type="number"
          name="year"
          label="Ano"
          placeholder="Ano"
          value={values.year}
          error={errors.year}
          touched={touched.year}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        <Input
          id="pagesNumber"
          type="number"
          name="pagesNumber"
          label="Número de páginas"
          placeholder="Número de páginas"
          value={values.pagesNumber}
          error={errors.pagesNumber}
          touched={touched.pagesNumber}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        <TextArea
          id="description"
          type="text"
          name="description"
          label="Descrição"
          placeholder="Descrição"
          value={values.description}
          error={errors.description}
          touched={touched.description}
          onChange={handleChange}
          onBlur={handleBlur}
        />

        <ButtonContainer>
          <SubmitButton type="submit">Cadastrar</SubmitButton>
        </ButtonContainer>
      </Form>
    </FormContainer>
  );
};

export default BooksForm;
