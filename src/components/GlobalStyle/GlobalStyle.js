import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    text-decoration: none;
    list-style: none;
  }
  
  body {
    font-family: Roboto, Helvetica, "Helvetica Neue", Arial, sans-serif;
    background: #e1e1e1;
  }
`;

export default GlobalStyle;
