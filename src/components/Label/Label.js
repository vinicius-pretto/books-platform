import React from "react";
import styled from "styled-components";

const LabelContainer = styled.label`
  font-size: 16px;
  margin-bottom: 5px;
  color: ${props => (props.hasError ? props.theme.error : props.theme.text)};
`;
LabelContainer.displayName = "LabelContainer";

const Label = ({ htmlFor, hasError, children }) => {
  return (
    <LabelContainer htmlFor={htmlFor} hasError={hasError}>
      {children}
    </LabelContainer>
  );
};

export default Label;
