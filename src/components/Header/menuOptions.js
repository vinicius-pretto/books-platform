import Routes from "../../router/Routes";

const menuOptions = {
  HOME: {
    title: "Livros",
    href: Routes.HOME
  },
  MY_BOOKS: {
    title: "Meus Livros",
    href: Routes.MY_BOOKS
  },
  BOOKS_REGISTER: {
    title: "Cadastro de livros",
    href: Routes.BOOKS_REGISTER
  },
  BOOKS: {
    title: "Livros cadastrados",
    href: Routes.BOOKS
  },
  LOGIN: {
    title: "Sair",
    href: Routes.LOGIN
  }
}

export default menuOptions;
