import React from "react";
import styled from "styled-components";

import Device from "../../../styles/Device";

const SearchForm = styled.form`
  display: flex;
  align-items: center;
  border: 1px solid #fff;
  padding: 0px 8px;
`;
SearchForm.displayName = "Search";

const SearchButton = styled.button`
  width: 20px;
  height: 20px;
  border: none;
  cursor: pointer;
  background: transparent;
  background-repeat: no-repeat;
  background-image: url("/icons/search.svg");
`;
SearchButton.displayName = "Search";

const SearchInput = styled.input`
  width: 180px;
  padding: 8px 6px;
  color: #c9c5c5;
  background: transparent;
  border: none;
  font-size: 14px;
  outline: none;

  @media ${Device.MOBILE} {
    width: 155px;
  }
`;
SearchInput.displayName = "SearchInput";

class Search extends React.Component {
  state = {
    searchFormOpen: false,
    searchInput: ""
  };

  onSubmit = e => {
    e.preventDefault();
    console.log("onSubmit", this.state.searchInput);
  };

  onSearchClick = () => {
    this.setState({ searchFormOpen: true });
  };

  componentDidUpdate() {
    if (this.refs.SearchInput) {
      this.refs.SearchInput.focus();
    }
  }

  onSearchInputBlur = () => {
    this.setState({ searchFormOpen: false });
  };

  onSearchInputChange = e => {
    this.setState({ searchInput: e.target.value });
  };

  render() {
    if (this.state.searchFormOpen) {
      return (
        <SearchForm onSubmit={this.onSubmit}>
          <SearchButton type="submit" aria-label="Buscar" title="Buscar" />
          <SearchInput
            ref="SearchInput"
            type="text"
            placeholder="Buscar por nome, autor"
            onBlur={this.onSearchInputBlur}
            value={this.state.searchInput}
            onChange={this.onSearchInputChange}
          />
        </SearchForm>
      );
    }
    return (
      <SearchButton
        type="submit"
        aria-label="Buscar"
        title="Buscar"
        onClick={this.onSearchClick}
      />
    );
  }
}

export default Search;
