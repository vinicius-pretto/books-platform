import styled from "styled-components";
import Device from "../../styles/Device";

const Container = styled.main`
  height: auto;
  width: auto;
  margin: 30px 65px;

  @media ${Device.LAPTOP_SMALL} {
    margin: 30px 20px;
  }
  @media ${Device.TABLET} {
    margin: 15px;
  }
`;
Container.displayName = "Container";

export default Container;
