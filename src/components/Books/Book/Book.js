import React from "react";
import styled from "styled-components";
import Device from "../../../styles/Device";
import { Link } from "react-router-dom";

const Container = styled.div`
  display: flex;
`;
Container.displayName = "Container";

const Cover = styled.img`
  max-width: 165px;
  max-height: 218px;

  @media ${Device.TABLET} {
    max-width: 144px;
    max-height: 198px;
  }
  @media ${Device.MOBILE} {
    max-width: 165px;
    max-height: 218px;
  }
  @media ${Device.MOBILE_SMALL} {
    max-width: 140px;
  }
`;
Cover.displayName = "Cover";

const BorrowButton = styled.button`
  text-transform: uppercase;
  padding: 15px 10px;
  background: ${props => props.theme.primary};
  border: 1px solid ${props => props.theme.primary};
  color: #fff;
  font-size: 13px;
  cursor: ${props => (props.isBorrowed ? "normal" : "pointer")};
  outline: none;
  opacity: ${props => (props.isBorrowed ? ".7" : "none")};
`;
BorrowButton.displayName = "BorrowButton";

const BookContainer = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 166px;
`;
BookContainer.displayName = "BookContainer";

const LinkContainer = styled(Link)`
  display: flex;
`;
LinkContainer.displayName = "LinkContainer";

const Book = ({ id, name, pictureUrl, isBorrowed, onClick }) => {
  return (
    <Container>
      <BookContainer>
        <LinkContainer to={`/books/${id}/details`}>
          <Cover src={pictureUrl} alt={name} />
        </LinkContainer>
        
        <BorrowButton
          type="button"
          aria-label="Reservar"
          onClick={onClick}
          disabled={isBorrowed}
          isBorrowed={isBorrowed}
        >
          Reservar
        </BorrowButton>
      </BookContainer>
    </Container>
  );
};

export default Book;
