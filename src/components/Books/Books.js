import React from "react";
import styled from "styled-components";

import Book from "./Book";
import Modal from '../Modal';

const BooksList = styled.ul`
  display: flex;
  align-items: flex-end;
  flex-wrap: wrap;
`;
BooksList.displayName = "BooksList";

const BookContainer = styled.li`
  display: flex;
  flex-direction: column;
  margin: 0px 2px 6px 2px;
`;
BookContainer.displayName = "BookContainer";

const Books = ({ books, isModalOpen, onConfirm, onCancel, onBookClick, deliveryDate }) => {
  return (
    <React.Fragment>
      <Modal 
        isOpen={isModalOpen}
        title="O livro foi reservado!"
        subtitle={`Deve ser entregue até a data ${deliveryDate}`}
        onConfirm={onConfirm}
        onCancel={onCancel}
      />
      <BooksList>
        {books.map((book, key) => (
          <BookContainer key={key}>
            <Book
              id={book.id}
              name={book.name}
              pictureUrl={book.pictureUrl}
              isBorrowed={book.isBorrowed}
              onClick={() => onBookClick(book.id)}
            />
          </BookContainer>
        ))}
      </BooksList>
    </React.Fragment>
  );
};

export default Books;
