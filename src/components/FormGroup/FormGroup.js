import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 20px;
`;
Container.displayName = "Container";

const FormGroup = ({ children }) => {
  return (
    <Container>{children}</Container>
  )
}

export default FormGroup;
