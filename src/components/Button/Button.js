import styled from "styled-components";

const Button = styled.button`
  background: ${props => props.primary ? props.theme.primary : '#e5e5e5'};
  color: ${props => props.primary ? '#fff' : props.theme.text};
  box-sizing: border-box;
  border: 1px solid ${props => props.primary ? props.theme.primary : '#b5b0b0'};
  font-size: 13px;
  border-radius: 4px;
  padding: 13px 20px;
  text-transform: uppercase;
  cursor: pointer;
  outline: none;
`;
Button.displayName = "Button";

export default Button;
