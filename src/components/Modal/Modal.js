import React from "react";
import styled, { keyframes } from "styled-components";
import Button from '../Button';

const Title = styled.h2`
  color: ${props => props.theme.text};
  font-size: 22px;
  margin-top: 10px;
  font-weight: normal;
`
Title.displayName = 'Title';

const Subtitle = styled.h3`
  color: ${props => props.theme.text};
  font-size: 15px;
  font-weight: normal;
  margin-top: 15px;
`
Subtitle.displayName = 'Subtitle';

const CloseIcon = styled.button`
  position: absolute;
  top: 20px;
  right: 20px;
  width: 17px;
  height: 17px;
  background: transparent;
  background-image: url("/icons/close.svg");
  background-repeat: no-repeat;
  cursor: pointer;
  border: none;
  outline: none;
`
CloseIcon.displayName = 'CloseIcon';

const ModalContainer = styled.div`
  position: fixed;
  display: ${props => props.isOpen ? 'flex' : 'none'};
  justify-content: center;
  align-items: center;
  width: 100vw;
  height: 100vh;
  top: 0;
  left: 0;
  z-index: 2000;
  background: rgba(0, 0, 0, .6);
`;
ModalContainer.displayName = "ModalContainer";

const animation = keyframes`
  from {
    opacity: 0;
    transform: translate3d(0, -60px, 0);
  }

  to {
    opacity: 1;
    transform: translate3d(0, 0, 0);
  }
`;

const StyledModal = styled.div`
  position: relative;
  flex-direction: column;
  justify-content: space-between;

  height: 280px;
  width: 32%;
  min-width: 300px;

  padding: 30px;
  background: #FFF;
  border: 1px solid #FFF;
  border-radius: 4px;
  animation: ${animation} .3s;
`
StyledModal.displayName = 'StyledModal';

const Footer = styled.div`
  position: absolute;
  bottom: 20px;
  right: 20px;
`
Footer.displayName = 'Footer';

const CancelButton = styled(Button)`
  margin-right: 5px;
`
CancelButton.displayName = 'CancelButon';

const Header = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  min-height: 60px;
`
Header.displayName = 'Header';

const Modal = ({ title, subtitle, warning, isOpen, onConfirm, onCancel }) => {
  return (
    <ModalContainer isOpen={isOpen}>
      <StyledModal>
        <CloseIcon type="button" onClick={onCancel} />
        
        <Header>
          {warning && <img src="/icons/warning.svg" aria-label="Warning" alt="Warning" />}
          <Title>{title}</Title>
          {subtitle && <Subtitle>{subtitle}</Subtitle>}
        </Header>

        <Footer>
          <CancelButton type="button" onClick={onCancel}>
            Cancelar
          </CancelButton>
          <Button type="button" onClick={onConfirm} primary>
            Continuar
          </Button>
        </Footer>
      </StyledModal>
    </ModalContainer>
  );
};

export default Modal;
