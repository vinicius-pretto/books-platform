import * as yup from "yup";

const validationSchema = yup.object().shape({
  name: yup
    .string()
    .min(3, 'O campo nome deve ter entre 3 e 80 caracters')
    .max(80, 'O campo nome deve ter entre 3 e 80 caracters')
    .required("O campo nome do livro não pode ser vazio"),
  author: yup
    .string()
    .min(3, 'O campo nome deve ter entre 3 e 30 caracters')
    .max(30)
    .required("O campo autor não pode ser vazio"),
  language: yup
    .string()
    .required("O campo idioma não pode ser vazio"),
  publisher: yup
    .string()
    .required("O campo editora não pode ser vazio"),
  year: yup
    .number()
    .required("O campo ano não pode ser vazio"),
  pagesNumber: yup
    .number()
    .required("O campo número de páginas não pode ser vazio"),
  description: yup
    .string()
    .min(3, 'O campo descrição deve ter no mínimo 3 caracteres')
    .required("O campo descrição não pode ser vazio"),
});

export default validationSchema;
