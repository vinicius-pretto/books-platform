import * as yup from "yup";

const validationSchema = yup.object().shape({
  firstName: yup
    .string()
    .min(3, "O campo nome deve ter entre 3 e 30 caracteres")
    .max(30, "O campo nome deve ter entre 3 e 30 caracteres")
    .required("O campo nome não pode ser vazio"),
  lastName: yup
    .string()
    .min(3, "O campo sobrenome deve ter entre 3 e 30 caracteres")
    .max(30, "O campo sobrenome deve ter entre 3 e 30 caracteres")
    .required("O campo sobrenome não pode ser vazio"),
  email: yup
    .string()
    .email("Por favor, insira um email válido")
    .required("O campo email não pode ser vazio"),
  birthDate: yup
    .string()
    .required("O campo data de nascimento não pode ser vazio"),
  telephone: yup.string().required("O campo telefone não pode ser vazio"),
  password: yup
    .string()
    .min(3, "O campo senha deve ter entre 3 e 30 caracteres")
    .max(30, "O campo senha deve ter entre 3 e 30 caracteres")
    .required("O campo senha não pode ser vazio")
});

export default validationSchema;
