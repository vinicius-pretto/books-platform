import React from "react";
import { connect } from "react-redux";

import Books from "../../components/Books";
import Container from "../../components/Container";
import Title from "../../components/Title";
import BooksService from "../../services/BooksService";
import homeActions from "../../redux/home/homeActions";

class Home extends React.Component {
  constructor() {
    super();
    this.booksService = new BooksService();
  }

  getBooks = async () => {
    const books = await this.booksService.getBooks();
    this.props.setBooks(books);
  };

  componentDidMount() {
    this.getBooks();
  }

  borrowBook = async bookId => {
    const deliveryDate = await this.booksService.borrowBook(bookId);
    await this.getBooks();
    this.props.openModal();
    this.props.setDeliveryDate(deliveryDate);
  };

  render() {
    const { books, isModalOpen, deliveryDate, closeModal } = this.props;

    return (
      <Container>
        <Title>Livros</Title>
        <Books
          books={books}
          onBookClick={this.borrowBook}
          isModalOpen={isModalOpen}
          deliveryDate={deliveryDate}
          onCancel={closeModal}
          onConfirm={closeModal}
        />
      </Container>
    );
  }
}

const stateToProps = state => {
  return {
    books: state.home.books,
    isModalOpen: state.home.isModalOpen,
    deliveryDate: state.home.deliveryDate
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setBooks: books => dispatch(homeActions.setBooks(books)),
    openModal: () => dispatch(homeActions.openModal()),
    closeModal: () => dispatch(homeActions.closeModal()),
    setDeliveryDate: deliveryDate =>
      dispatch(homeActions.setDeliveryDate(deliveryDate))
  };
};

export default connect(
  stateToProps,
  mapDispatchToProps
)(Home);
