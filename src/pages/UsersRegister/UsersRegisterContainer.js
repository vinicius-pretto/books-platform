import React from 'react';
import { Formik } from 'formik';

import UsersRegister from './UsersRegister';
import userSchema from '../../validations/userSchema';
import UsersService from '../../services/UsersService';
import history from '../../router/history';
import Routes from '../../router/Routes';
import UserRole from '../../userRole';

class UsersRegisterContainer extends React.Component {
  constructor() {
    super();
    this.initialValues = {
      firstName: '',
      lastName: '',
      email: '',
      birthDate: '',
      telephone: '',
      password: ''
    }
    this.usersService = new UsersService();
  }

  onSubmit = async values => {
    const user = {
      firstName: values.firstName,
      lastName: values.lastName,
      email: values.email,
      birthDate: values.birthDate,
      telephone: values.telephone,
      password: values.password,
      role: UserRole.USER
    }
    await this.usersService.createUser(user);
    history.push(Routes.LOGIN);
  }

  render() {
    return (
      <Formik
        initialValues={this.initialValues}
        onSubmit={this.onSubmit}
        validationSchema={userSchema}
      >
        {props => {
           const {
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit
          } = props;

          return (
            <UsersRegister 
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
              handleSubmit={handleSubmit}
            />
          )
        }}
      </Formik>
    )
  }
}

export default UsersRegisterContainer;
