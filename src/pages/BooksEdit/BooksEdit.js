import React from "react";

import BooksForm from "../../components/BooksForm";

class BooksEdit extends React.Component {
  componentDidMount() {
    setTimeout(() => {
      const { bookToEdit } = this.props;
      const values = {
        name: bookToEdit.name,
        author: bookToEdit.author,
        language: bookToEdit.language,
        publisher: bookToEdit.publisher,
        year: bookToEdit.year,
        pagesNumber: bookToEdit.year,
        description: bookToEdit.description
      }
      this.props.setValues(values);
    }, 800)
  }
  
  render() {
    const {
      values,
      errors,
      touched,
      handleChange,
      handleBlur,
      handleSubmit,
      onSubmit,
      onChangeFile,
      uploadedFile
    } = this.props;

    return (
      <BooksForm
        onSubmit={onSubmit}
        onChangeFile={onChangeFile}
        uploadedFile={uploadedFile}
        values={values}
        errors={errors}
        touched={touched}
        handleChange={handleChange}
        handleBlur={handleBlur}
        handleSubmit={handleSubmit}
      />
    );
  }
}

export default BooksEdit;
