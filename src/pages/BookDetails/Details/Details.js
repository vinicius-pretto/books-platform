import React from "react";
import styled from "styled-components";
import Device from "../../../styles/Device";

const Container = styled.div`
  margin-left: 18px;

  @media ${Device.MOBILE} {
    margin: 10px 0 0 0;
  }
`;
Container.displayName = "Container";

const BookTitle = styled.h2`
  margin: 0;
  font-size: 23px;
  font-weight: normal;
  color: #222;

  @media ${Device.MOBILE} {
    font-size: 20px;
  }
`;
BookTitle.displayName = "BookTitle";

const AuthorName = styled.div`
  margin-top: 4px;
  color: ${props => props.theme.text};
`;
AuthorName.displayName = "AuthorName";

const Description = styled.p`
  color: ${props => props.theme.text};
`;
Description.displayName = "Description";

const PagesNumber = styled.div`
  color: ${props => props.theme.text};
`;
PagesNumber.displayName = "PagesNumber";

const Publisher = styled.div`
  color: ${props => props.theme.text};
`;
Publisher.displayName = "Publisher";

const Language = styled.div`
  color: ${props => props.theme.text};
`;
Language.displayName = "Language";

const Divider = styled.hr`
  margin: 20px 0;
  border-top: 1px solid #a5a2a2;
  min-width: 40vw;

  @media ${Device.MOBILE} {
    margin: 15px 0;
  }
`;
Divider.displayName = "Divider";

const Details = ({ selectedBook }) => {
  return (
    <Container>
      <BookTitle>{selectedBook.name}</BookTitle>
      <AuthorName>por {selectedBook.author} (Autor)</AuthorName>
      <Divider />

      <Description>{selectedBook.description}</Description>
      <Divider />

      <PagesNumber>{selectedBook.pagesNumber} páginas</PagesNumber>
      <Publisher>Editora: {selectedBook.publisher}</Publisher>
      <Language>Idioma: {selectedBook.language}</Language>
    </Container>
  );
};

export default Details;
