import React from "react";
import styled from 'styled-components';

import Container from '../../components/Container';
import Title from '../../components/Title';
import BooksService from '../../services/BooksService';
import Book from '../../components/Books/Book';
import Device from '../../styles/Device';
import Details from './Details';

const BookContainer = styled.div`
  display: flex;

  @media ${Device.MOBILE} {
    flex-direction: column;
  }
`
BookContainer.displayName = 'BookContainer';

class BookDetails extends React.Component {
  constructor() {
    super();
    this.booksService = new BooksService();
    this.state = {
      selectedBook: {}
    }
  }

  async componentDidMount() {
    const bookId = this.props.match.params.id;
    const selectedBook = await this.booksService.getBookById(bookId);
    this.setState({ selectedBook });
  }

  render() {
    const { selectedBook } = this.state;

    return (
      <Container>
        <Title>Detalhes do Livro</Title>

        <BookContainer>
          <Book 
            id={selectedBook.id}
            name={selectedBook.name}
            pictureUrl={selectedBook.pictureUrl}
          />
          <Details selectedBook={selectedBook} />
        </BookContainer>
      </Container>
    )
  }
};

export default BookDetails;
