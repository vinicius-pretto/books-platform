import React from 'react';
import { connect } from 'react-redux';

import Login from './Login';
import { Formik } from 'formik';
import validationSchema from '../../validations/loginSchema';
import AuthService from '../../services/AuthService';
import loginActions from "../../redux/login/loginActions";
import Routes from '../../router/Routes';
import history from '../../router/history';

class LoginContainer extends React.Component {
  constructor() {
    super();
    this.initialValues = {
      username: '',
      password: ''
    }
    this.state = {
      hasError: ''
    }
    this.authService = new AuthService();
  }

  componentDidMount() {
    this.authService.destroySession();
    this.props.signOut();
  }

  onSubmit = async values => {
    try {
      const credentials = {
        username: values.username,
        password: values.password
      }
      const response = await this.authService.auth(credentials);
      const isAuthenticated = this.authService.isAuthenticated();
      this.props.setSession(response.user, isAuthenticated);
      history.push(Routes.HOME);
    } catch (error) {
      this.setState({ hasError: true });
    }
  }
  
  render() {
    return (
      <Formik
        initialValues={this.initialValues}
        onSubmit={this.onSubmit}
        validationSchema={validationSchema}
      >
        {props => {
          const {
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit
          } = props;
         
          return (
            <Login 
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
              handleSubmit={handleSubmit}
              hasError={this.state.hasError}
            />
          )
        }}
      </Formik>
    )
  }
}

const stateToProps = state => {
  return {
    isAuthenticated: state.login.isAuthenticated,
    currentUser: state.login.currentUser
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setSession: (currentUser, isAuthenticated) =>
      dispatch(loginActions.setSession(currentUser, isAuthenticated)),
    signOut: () => dispatch(loginActions.signOut())
  };
};

export default connect(
  stateToProps,
  mapDispatchToProps
)(LoginContainer);
