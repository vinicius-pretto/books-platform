import React from "react";
import { connect } from 'react-redux';

import Container from "../../components/Container";
import Title from "../../components/Title";
import Books from "../../components/Books";
import BooksService from "../../services/BooksService";
import AuthService from "../../services/AuthService";
import myBooksActions from '../../redux/myBooks/myBooksActions';

class MyBooks extends React.Component {
  constructor() {
    super();
    this.booksService = new BooksService();
    this.authService = new AuthService();
  }

  async componentDidMount() {
    const user = this.authService.getCurrentUser();
    const books = await this.booksService.getBorrowedBooks(user.id);
    this.props.setBooks(books);
  }

  render() {
    return (
      <Container>
        <Title>Meus Livros</Title>
        <Books books={this.props.books} />
      </Container>
    );
  }
}

const stateToProps = state => {
  return {
    books: state.myBooks.books
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setBooks: books => dispatch(myBooksActions.setBooks(books))
  }
}

export default connect(stateToProps, mapDispatchToProps)(MyBooks);
