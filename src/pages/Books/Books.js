import React from "react";
import styled from "styled-components";
import { Link } from 'react-router-dom';

import Container from "../../components/Container";
import Modal from "../../components/Modal";
import Title from '../../components/Title';

const TableContainer = styled.div`
  padding: 20px;
  background: #fff;
  border-radius: 4px;
`;
TableContainer.displayName = "TableContainer";

const Table = styled.table`
  border-collapse: collapse;
  border-spacing: 0;
  border-color: #a5a2a2;
  font-size: 14px;
  width: 100%;
  color: ${props => props.theme.text};
  font-family: Roboto, Helvetica, "Helvetica Neue", Arial, sans-serif;
  background-color: transparent;
`;
Table.displayName = "Table";

const Thead = styled.thead`
  display: table-row-group;
  vertical-align: middle;
  border-color: inherit;
`;
Thead.displayName = "Thead";

const ThCell = styled.th`
  border-style: solid;
  border-width: 0;
  border-color: #a5a2a2;
  text-align: left;
  font-weight: bold;
  padding: 12px;
`;
ThCell.displayName = "ThCell";

const Tbody = styled.tbody`
  display: table-row-group;
  vertical-align: middle;
  border-color: inherit;
`;
Tbody.displayName = "Tbody";

const Row = styled.tr`
  display: table-row;
  vertical-align: inherit;
  border-color: inherit;
`;
Row.displayName = "Row";

const Cell = styled.td`
  border-style: solid;
  border-width: 1px 0 0 0;
  border-color: #a5a2a2;
  text-align: left;
  padding: 12px;
  vertical-align: top;
`;
Cell.displayName = "Cell";

const EditLink = styled(Link)`
  display: block;
  width: 30px;
  height: 30px;
  border: none;
  cursor: pointer;
  background: transparent;
  background-repeat: no-repeat;
  background-image: url("/icons/pencil.svg");
`;
EditLink.displayName = "EditLink";

const RemoveButton = styled.button`
  width: 30px;
  height: 30px;
  border: none;
  cursor: pointer;
  background: transparent;
  background-repeat: no-repeat;
  background-image: url("/icons/trash.svg");
`;
RemoveButton.displayName = "RemoveButton";

const Books = ({ books, isModalOpen, onRemoveButtonClick, onRemoveBook, onCloseModal }) => {
  return (
    <React.Fragment>
      <Modal
        warning
        isOpen={isModalOpen}
        onConfirm={onRemoveBook}
        onCancel={onCloseModal}
        title="Deseja realmente excluir este livro?"
      />

      <Container>
        <Title>Livros Cadastrados</Title>

        <TableContainer>
          <Table>
            <Thead>
              <Row>
                <ThCell>Nome</ThCell>
                <ThCell>Autor</ThCell>
                <ThCell>Idioma</ThCell>
                <ThCell>Editora</ThCell>
                <ThCell>Páginas</ThCell>
                <ThCell>Descrição</ThCell>
                <ThCell>Emprestado</ThCell>
                <ThCell>Editar</ThCell>
                <ThCell>Excluir</ThCell>
              </Row>
            </Thead>
            <Tbody>
              {books.map((book, key) => (
                <Row key={key}>
                  <Cell>{book.name}</Cell>
                  <Cell>{book.author}</Cell>
                  <Cell>{book.language}</Cell>
                  <Cell>{book.publisher}</Cell>
                  <Cell>{book.pagesNumber}</Cell>
                  <Cell>{book.description}</Cell>
                  <Cell>{book.isBorrowed ? 'Sim' : 'Não'}</Cell>
                  <Cell>
                    <EditLink to={`/books/${book.id}/edit`} />
                  </Cell>
                  <Cell>
                    <RemoveButton type="button" onClick={() => onRemoveButtonClick(book.id)} />
                  </Cell>
                </Row>
              ))}
            </Tbody>
          </Table>
        </TableContainer>
      </Container>
    </React.Fragment>
  );
};

export default Books;
