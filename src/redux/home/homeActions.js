import types from "./types";

function setBooks(books) {
  return { type: types.SET_BOOKS, payload: { books } };
}

function openModal() {
  return { type: types.OPEN_MODAL };
}

function closeModal() {
  return { type: types.CLOSE_MODAL };
}

function setDeliveryDate(deliveryDate) {
  return { type: types.SET_DELIVERY_DATE, payload: { deliveryDate } };
}

export default {
  setBooks,
  openModal,
  closeModal,
  setDeliveryDate
};
