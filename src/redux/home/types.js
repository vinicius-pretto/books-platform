const types = {
  SET_BOOKS: 'ebooks-library/home/set-books',
  OPEN_MODAL: 'ebooks-library/home/open-modal',
  CLOSE_MODAL: 'ebooks-library/home/close-modal',
  SET_DELIVERY_DATE: 'ebooks-library/home/set-delivery-date'
}

export default types;
