import { createStore, combineReducers } from 'redux';
import homeReducer from './home';
import loginReducer from './login';
import myBooksReducer from './myBooks';

const reducers = combineReducers({
  home: homeReducer,
  login: loginReducer,
  myBooks: myBooksReducer
});

const store = createStore(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;
