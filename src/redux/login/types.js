const types = {
  SET_SESSION: 'ebooks-library/login/set-session',
  SIGN_OUT: 'ebooks-library/login/sign-out'
}

export default types;
