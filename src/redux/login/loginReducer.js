import types from './types';

const initialState = {
  currentUser: {},
  isAuthenticated: false
}

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.SET_SESSION:
      return {
        ...state,
        currentUser: action.payload.currentUser,
        isAuthenticated: action.payload.isAuthenticated
      };
    case types.SIGN_OUT:
      return {
        ...state,
        currentUser: initialState.currentUser,
        isAuthenticated: initialState.isAuthenticated
      };
    default:
      return state;
  }
}
