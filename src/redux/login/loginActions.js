import types from "./types";

function setSession(currentUser, isAuthenticated) {
  return { type: types.SET_SESSION, payload: { currentUser, isAuthenticated } };
}

function signOut() {
  return { type: types.SIGN_OUT };
}

export default {
  setSession,
  signOut
};
