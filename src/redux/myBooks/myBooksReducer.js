import types from './types';

const initialState = {
  books: []
}

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.SET_BOOKS:
      return {
        ...state,
        books: action.payload.books
      };
    default:
      return state;
  }
}
