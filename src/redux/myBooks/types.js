const types = {
  SET_BOOKS: 'ebooks-library/myBooks/set-books'
}

export default types;
