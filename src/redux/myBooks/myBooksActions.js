import types from './types';

function setBooks(books) {
  return { type: types.SET_BOOKS, payload: { books } };
}

export default {
  setBooks
}
