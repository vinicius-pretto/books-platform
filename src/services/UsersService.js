import axios from "axios";
import config from "../config";

class UsersService {
  constructor() {
    this.httpClient = axios.create({
      baseURL: config.apiHost,
    });
  }

  createUser(user) {
    return this.httpClient.post('/v1/users', user);
  }
}

export default UsersService;
