import axios from "axios";
import _isEmpty from "lodash/isEmpty";

import config from "../config";

const ACCESS_TOKEN = "x-access-token";
const USER = "user";

class AuthService {
  removeAccessToken() {
    window.sessionStorage.removeItem(ACCESS_TOKEN);
  }

  getAccessToken() {
    return window.sessionStorage.getItem(ACCESS_TOKEN);
  }

  getCurrentUser() {
    try {
      const user = window.sessionStorage.getItem(USER);
      const userJson = JSON.parse(user);
      return userJson || {};
    } catch (error) {
      console.error(error);
      return {};
    }
  }

  isAuthenticated() {
    const currentUser = this.getCurrentUser();
    return  !_isEmpty(currentUser);
  }

  async auth(credentials) {
    const response = await axios.post(`${config.apiHost}/v1/auth`, credentials);
    const user = response.data;
    window.sessionStorage.setItem(USER, JSON.stringify(user));
    return { user };
  }

  destroySession() {
    window.sessionStorage.removeItem(ACCESS_TOKEN);
    window.sessionStorage.removeItem(USER);
  }
}

export default AuthService;
