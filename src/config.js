const config = {
  apiHost: process.env.REACT_APP_API_HOST,
  booksPlatformServiceHost: process.env.REACT_APP_BOOKS_PLATFORM_SERVICE_HOST
};

export default config;
