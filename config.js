const config = {
  port: process.env.PORT || 8080,
  staticFolder: process.env.STATIC_FOLDER || "build",
  staticConfig: {}
};

module.exports = config;
